# BGTM - Engine for compiling a JavaScript file.

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of processing a list of JavaScript files into a single
bundled JS file.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-js

In your gulpfile add the following dependency:

    var js = require('bgtm-engine-js');

Then add the following task.

    tm.add('js', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/js/**/*'
        ],
        liveReload: true,
        engine: js,
        engineOptions: {
            'src': [
                'src/js/plugins/**/*.js',
                'src/js/main.js'
            ],
            'dest': 'dest/js/main.min.js',
            'outputFileName': 'main.js'
        }
    });

# What it does

* Gets the source files specified in the `src` property.
* Generates a single file using
  [gulp-concat](https://www.npmjs.com/package/gulp-concat).
* Changes the extension of the `outputFileName` option to end with
  `.min.js` for the minified version of the file.
* Minifies the file using
  [gulp-uglify](https://www.npmjs.com/package/gulp-uglify)
* Generates the sourcemaps file for the minified version of the file.
* Outputs the result to the destination specified in the `dest` property.

Files in the destination for the `outputFileName` of `main.js` would be:

    - main.min.js
    - main.min.js.map

# Engine Options

## Defaults

    {
        src: '',
        dest: '',
        outputFileName: 'main.js'
    }

| Option         | Mandatory | Description |
|----------------|-----------|-------------|
| src            | yes       | The source path to look for changes, This could be a string or an array of paths which can be patterns as well as paths. |
| dest           | yes       | The destination file for the output to be placed into. |
| outputFileName | yes       | The name of the file to be output to the destination folder.. |

# License

Copyright 2017 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.