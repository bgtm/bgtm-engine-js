var plumber = require('gulp-plumber');
var uglifyjs = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var deepExtend = require('deep-extend');

/**
 * This the default engine for processing a list of JavaScript files into a single bundled JS file.
 * This ensures consistent build practices across many developers within a team.
 *
 * @param taskManager
 * @param args
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        src: '',
        dest: '',
        outputFileName: 'main.js'
    };
    var engineOptions = deepExtend({}, defaults, args);

    var uglifyOptions = {
        // There are no default options that we are setting, but maybe in a later version.
    };

    var pipeline = this.src(engineOptions.src)
        .pipe(plumber())
        .pipe(taskManager.IncrementalGet())
        .pipe(sourcemaps.init());

    if (taskManager.isEnvironment(taskManager.environment.PRODUCTION)) {
        pipeline = pipeline.pipe(uglifyjs(uglifyOptions));
    }
    
        
    pipeline = pipeline
        .pipe(taskManager.IncrementalSet())
        .pipe(concat(engineOptions.outputFileName))
        .pipe(rename({extname: '.min.js'}))
        .pipe(sourcemaps.write('./'))
        .pipe(this.dest(engineOptions.dest))
        .pipe(done());
        
    return pipeline;
};